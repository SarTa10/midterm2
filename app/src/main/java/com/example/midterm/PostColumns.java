package com.example.midterm;

public class PostColumns {

    public static final String TABLE_NAME= "POSTS";
    public static final String ID= "id";
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String EMAIL = "email";
    public static final String REVIEW = "review";
}
