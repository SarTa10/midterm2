package com.example.midterm;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText review;
    private Button Send;
    private sqliteHelper DataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        init();

        sendReviewListener();
    }

    private void init() {
        firstName =findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        email = findViewById(R.id.email);
        review = findViewById(R.id.review);
        Send = findViewById(R.id.send);
        DataBase = new sqliteHelper(MainActivity.this);
    }

    private Boolean checkEmail(String mail) {
        return mail.contains("@") && mail.contains(".") && mail.length()>5;
    }

    private void sendReviewListener() {

        Send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                String fn = firstName.getText().toString();
                String ln = lastName.getText().toString();
                String mail = email.getText().toString();
                String Review = review.getText().toString();


                if(fn.length() <2 || ln.length() <3 || !checkEmail(mail) || Review.length() <5) {
                    Toast.makeText(MainActivity.this, "insert valid information", Toast.LENGTH_SHORT).show();
                    return;
                }

                DataBase.insert(fn,ln,mail,Review);
            }
        });


    }
}