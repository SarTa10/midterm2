package com.example.midterm;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class sqliteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "post.db";
    private static final String VERSION = "1";

    private static final String SQLITE_CREATE_TABLE = "CREATE TABLE " + PostColumns.TABLE_NAME + " ( "
            + PostColumns.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PostColumns.FIRSTNAME + " Text, "
            + PostColumns.LASTNAME + " Text, "
            + PostColumns.EMAIL + " Text, "
            + PostColumns.REVIEW + " Text )";

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + PostColumns.TABLE_NAME;
    public sqliteHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, Integer.parseInt(VERSION));
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQLITE_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(SQL_DELETE_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void insert(String firstName, String lastName, String email, String review){
        ContentValues cv = new ContentValues();
        cv.put(PostColumns.FIRSTNAME, firstName);
        cv.put(PostColumns.LASTNAME, lastName);
        cv.put(PostColumns.EMAIL, email);
        cv.put(PostColumns.REVIEW, review);

        getWritableDatabase().insert(PostColumns.TABLE_NAME, null, cv);
    }

    public void delete(long id) {
        String where = PostColumns.ID + " = ?";
        String[] args = new String[]{
            String.valueOf(id)
        };

        getWritableDatabase().delete(PostColumns.TABLE_NAME,where,args);
    }

    public void update(long id, String firstName, String lastName, String email, String review) {
        ContentValues cv = new ContentValues();
        cv.put(PostColumns.FIRSTNAME, firstName);
        cv.put(PostColumns.LASTNAME, lastName);
        cv.put(PostColumns.EMAIL, email );
        cv.put(PostColumns.REVIEW, review);

        String where = PostColumns.ID + " = ?";
        String[] args = new String[]{
                String.valueOf(id)
        };

        getWritableDatabase().update(
                PostColumns.TABLE_NAME,
                cv,
                where,
                args
        );
    }
}
